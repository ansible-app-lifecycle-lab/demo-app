# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM node:19.1 as build
# Set the working directory
WORKDIR /app
COPY . .

# Generate the build of the application (cleanup just in case)
RUN npm i --force && \
    npm run build --omit=dev

# Stage 2: Configure nginx to run webapp
FROM registry.access.redhat.com/ubi8/nginx-120

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /code

COPY --from=build /app/dist/demo-app /usr/share/nginx/html

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]

# Demo App

This project contains an Angular 15 web named **demo-app**.

![app image](app-image.png)

## Run locally

```sh
# Open new terminal and start ng server (live update for coding)
ng serve

# Review application in a browser: http://localhost:4200
```

## Run locally as a Container

```sh
# Build Container using podman
podman build --tag demo-app:1.0.0 .

# Start Container
podman run --name demo -d -p 81:8080 demo-app:1.0.0

# Review application in a browser: http://localhost:81
```

## Run on OpenShift

Create application:

```sh
# Create application
oc new-app --name=demo-app --strategy=docker https://gitlab.com/ansible-app-lifecycle-lab/demo-app

# Deploy and follow logs until image is created
oc logs bc/demo-app -f

# Expose service
oc expose svc demo-app

# Get URL
oc get route demo-app -o jsonpath='{.status.ingress[0].host}'

# Test in your browser
```

Configure application information:

```sh
# Create configuration map
cat << EOF | oc apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: demo-app-info
data:
  info.json: |
    { 
      "chartInfo": "chart-not-used",
      "appVersion": "demo-app-1.0.0"
    }  
EOF
```

Update the deployment:

```sh
# Deploy applications
oc set volume deploy/demo-app --add --type=configmap --configmap-name=demo-app-info --mount-path=/usr/share/nginx/html/assets/config

# Test in your browser
```

## Build Image

```sh
# Build image
podman build -t demo-app:1.0.0 .

# Login into quay
podman login quay.io

# Tag and push into quay
podman tag demo-app:1.0.0 quay.io/ansible_helm/demo-app:1.0.0
podman push quay.io/ansible_helm/demo-app:1.0.0
```